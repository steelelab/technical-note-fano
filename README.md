# Technical Note Fano

A technical note Gary has written up explaining some of our insights into the origin of "fano" lineshaapes, and also pointing out the problem that cross talk from the input to output port will result in systematic offsets of the quadratures which make it impossible to determine the internal quality factor without uncontrolled systematic error.
